from dnnv.properties import *
import numpy as np

N = Network("N")


x_min = np.array([0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0])
x_max = np.array([0 , 0 , 0 , 0 , 1 , 1 , 1 , 0 , 0 , 0 , 1])


Forall(
	x, Implies(x_min <= x <= x_max, Or(N(x)[(0,0)] > N(x)[(0,1)]))
)



