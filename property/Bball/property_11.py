from dnnv.properties import *
import numpy as np

N = Network("N")

x_min = np.array([0, 0, 0])
x_max = np.array([1, 0, 1])

#y_param: x, v, time


y_max = 11.10


Forall(
	x, Implies(x_min <= x <= x_max, N(x)[(0,1)] < y_max)
)



