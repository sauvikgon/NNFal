
t__ = [0; 5; 10; 15; 20; 25; 30; 35; 40; 45; 50; 55; 60; 65; 70; 75; 80; 85; 90; 95; 100];
u__ = [th0 br0; th0 br0; th0 br0; th0 br0; th1 br1; th1 br1; th1 br1; th1 br1; th2 br2; th2 br2; th2 br2; th2 br2; th3 br3; th3 br3; th3 br3; th3 br3; th4 br4; th4 br4; th4 br4; th4 br4; th5 br5];


u = [t__, u__];
T = 100;


assignin('base','u',u);
assignin('base','T',T);

result = sim('cars', ...
  'StopTime', 'T', ...
  'LoadExternalInput', 'on', 'ExternalInput', 'u', ...
  'SaveTime', 'on', 'TimeSaveName', 'tout', ...
  'SaveOutput', 'on', 'OutputSaveName', 'yout', ...
  'SaveFormat', 'Array');
  
tout = result.tout;
yout = result.yout;  
index = find(tout(:,1) == 0);
writematrix(yout(index:end,:),'yout.csv');
disp('done executions')
%end
