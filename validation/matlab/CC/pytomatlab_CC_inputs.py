import matlab.engine
import os
import sys


eng = matlab.engine.start_matlab()

current_directory = os.getcwd()
os.chdir("../validation/matlab/CC")
current_directory = os.getcwd()
folder_path2 = current_directory + "/chasing-cars"
eng.addpath(folder_path2)
os.chdir(current_directory+"/chasing-cars")

eng.workspace['th0'] = float(sys.argv[1])
eng.workspace['br0'] = float(sys.argv[2])
eng.workspace['th1'] = float(sys.argv[3])
eng.workspace['br1'] = float(sys.argv[4])
eng.workspace['th2'] = float(sys.argv[5])
eng.workspace['br2'] = float(sys.argv[6])
eng.workspace['th3'] = float(sys.argv[7])
eng.workspace['br3'] = float(sys.argv[8])
eng.workspace['th4'] = float(sys.argv[9])
eng.workspace['br4'] = float(sys.argv[10])
eng.workspace['th5'] = float(sys.argv[11])
eng.workspace['br5'] = float(sys.argv[12])


fun_name = 'run_CC_inputs'
eng.feval(fun_name, nargout=0)

eng.quit()
