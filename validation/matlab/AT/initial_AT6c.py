import matlab.engine
import os
import sys
import pandas as pd


arguments = sys.argv[1:]
eng = matlab.engine.start_matlab()

current_directory1 = os.getcwd()
os.chdir("../validation/matlab/AT")
current_directory = os.getcwd()
eng.addpath(current_directory)
os.chdir(current_directory)

eng.workspace['th0'] = float(arguments[0])
eng.workspace['br0'] = float(arguments[1])
eng.workspace['th1'] = float(arguments[2])
eng.workspace['br1'] = float(arguments[3])
eng.workspace['th2'] = float(arguments[4])
eng.workspace['br2'] = float(arguments[5])
eng.workspace['th3'] = float(arguments[6])
eng.workspace['br3'] = float(arguments[7])

fun_name = "run_AT"
eng.feval(fun_name, nargout=0)

os.chdir(current_directory1)

df1 = pd.read_csv('yout.csv', names=["v", "w", "g"])
V = df1.loc[0:2000]
W = df1.loc[0:3000]
count=len(W[W['w'] >= 3000])
count2 = len(V[V['v'] > 65])
if (count == 0):
	if (count2):
		print("Validated")
	else:
		print("NOT FOUND")
else:
	print("NOT FOUND")


eng.quit()
