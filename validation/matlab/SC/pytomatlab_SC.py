import matlab.engine
import os
import sys


eng = matlab.engine.start_matlab()


current_directory = os.getcwd()
os.chdir("../validation/matlab/SC")
current_directory = os.getcwd()
eng.addpath(current_directory)
os.chdir(current_directory)

eng.workspace['S0'] = float(sys.argv[1])
eng.workspace['S1'] = float(sys.argv[2])
eng.workspace['S2'] = float(sys.argv[3])
eng.workspace['S3'] = float(sys.argv[4])
eng.workspace['S4'] = float(sys.argv[5])
eng.workspace['S5'] = float(sys.argv[6])
eng.workspace['S6'] = float(sys.argv[7])
eng.workspace['S7'] = float(sys.argv[8])
eng.workspace['S8'] = float(sys.argv[1])
eng.workspace['S9'] = float(sys.argv[2])
eng.workspace['S10'] = float(sys.argv[3])
eng.workspace['S11'] = float(sys.argv[4])
eng.workspace['S12'] = float(sys.argv[5])
eng.workspace['S13'] = float(sys.argv[6])
eng.workspace['S14'] = float(sys.argv[7])
eng.workspace['S15'] = float(sys.argv[8])
eng.workspace['S16'] = float(sys.argv[1])
eng.workspace['S17'] = float(sys.argv[2])
eng.workspace['S18'] = float(sys.argv[3])
eng.workspace['S19'] = float(sys.argv[4])
eng.workspace['S20'] = float(sys.argv[5])

fun_name = "run_SC"
eng.feval(fun_name, nargout=0)

eng.quit()
