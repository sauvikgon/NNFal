
t__ = [0; 1.75; 3.5; 5.25; 7; 8.75; 10.5; 12.25; 14; 15.75; 17.5; 19.25; 21; 22.75; 24.5; 26.25; 28; 29.75; 31.5; 33.25; 35];
u__ = [S0; S1; S2; S3; S4; S5; S6; S7; S8; S9; S10; S11; S12; S13; S14; S15; S16; S17; S18; S19; S20];

u = [t__, u__];
T = 35;
assignin('base','ExternalInput',u);
assignin('base','StopTime',T); % cannot re-use name T here

result = sim('steamcondense_RNN_22', ...
  'StopTime', 'StopTime', ...
  'LoadExternalInput', 'on', 'ExternalInput', 'ExternalInput', ...
  'SaveTime', 'on', 'TimeSaveName', 'tout', ...
  'SaveOutput', 'on', 'OutputSaveName', 'yout', ...
  'SaveFormat', 'Array');
tout = result.tout;
yout = result.yout;  
index = find(tout(:,1) == 30);
writematrix(yout(index:end,:),'yout.csv');
disp('Success')
